
protocol GameRules {
    
    init(board: Board)
    
    func winner() -> Player
    
}

struct Connect3Rules : GameRules {
    
    let _board : Board
    let _MaxCoins = 3
    
    init(board: Board) {
        _board = board
    }
    
    func winner() -> Player {
        
        // En columnas
        for player in [Player.Red, Player.White]{
            if winnerInColumns(player: player){
                return player
            }
        }
        
        // En filas
        
        // En diagonales
        
        return .Empty
    }
    
    func winnerInColumns(player: Player) -> Bool{
        
        for column in 0..<Board.width{
            if winner(player: player,
                      column: column){
                return true
            }
        }
        return false
        
    }
    
    
    func winner(player: Player, column: Int)->Bool{
        
        guard let col = _board[column] else{
            return false
        }
        var total = 0
        
        // helper function
        func isAWin() -> Bool{
            if total >= _MaxCoins{
                return true
            }else{
                return false
            }
        }
        
        for currentPlayer in col{
            
            if currentPlayer == player{
                total += 1
                // comprobar que es una victoria
                if isAWin(){
                    break
                }
            }else{
                total = 0
            }
        }
        return isAWin()
        
        
        
    }
    
}







