
// (red, red, white, empty)
// Columna con los jugadores empezando desde abajo
struct Board{
    
    typealias BoardColumn = [Player]
    
    static let width = 5
    static let heigh = 5
    
    var _boardRepresentation : [BoardColumn]
    
    init() {
        // Create the 5x5 board
        _boardRepresentation = [[.Empty, .Empty, .Empty, .Empty, .Empty],
                  [.Empty, .Empty, .Empty, .Empty, .Empty],
                  [.Empty, .Empty, .Empty, .Empty, .Empty],
                  [.Empty, .Empty, .Empty, .Empty, .Empty],
                  [.Empty, .Empty, .Empty, .Empty, .Empty]
        ]
    }
    
    
    
    mutating func playAt(col: Int, player: Player){
        
        guard col >= 0 && col < Board.width else {
            return
        }
        
        let column = _boardRepresentation[col]
        
        for (index, currentPlayer) in column.enumerated(){
            if currentPlayer == .Empty{
                //metemos la ficha
                _boardRepresentation[col][index] = player
                break
            }
        }
    }
    
    
    
    
    func playerAt(col: Int, row: Int) -> Player{
        
        guard col >= 0 && col <= Board.width else {
            return .Empty
        }
        
        guard row >= 0 && row <= Board.heigh else {
            return .Empty
        }
        
        return _boardRepresentation[col][row]
        
    }
    
}

//MARK: - Extensions

// proxies
extension Board{
    
    var proxyForEquality : [Player]{
        get{
            return _boardRepresentation.flatMap{$0}
        }
    }
    
    var proxyForHashValue : Int{
        get{
            
            var proxy = [Int]()
            var newVal = 0
            for player in proxyForEquality{
                if player == .Red{
                    newVal = 0
                }
                if player == .White{
                    newVal = 1
                }
                if player == .Empty{
                    newVal = 2
                }
                proxy.append(newVal)
            }
            return proxy.reduce(0, { (a, b) -> Int in
                a+b
            })
        }
    }
    
}
extension Board : Equatable{
    
    public static func ==(lhs: Board, rhs: Board) -> Bool{
        return lhs.proxyForEquality == rhs.proxyForEquality
    }
    
}

extension Board : Hashable{
    
    public var hashValue: Int{
        get{
            return self.proxyForHashValue
        }
    }
}

//MARK: - Accessors
extension Board{
    
    subscript (column: Int) -> BoardColumn?{
        
        guard column >= 0 && column < Board.width else{
            return nil
        }
        return _boardRepresentation[column]
    }
}


//MARK: - Transformations
extension Board{
    
    init(boardRepresentation: [BoardColumn] ) {
        _boardRepresentation = boardRepresentation
    }
    
    func transposed() -> Board{
        
        var aux = Player.Empty
        var transposedRepresentation = _boardRepresentation
        
        for i in 0..<Board.width{
            for j in 0..<Board.heigh{
                aux = transposedRepresentation[i][j]
                transposedRepresentation[i][j] = transposedRepresentation[j][i]
                transposedRepresentation[j][i] = aux
            }
        }
        
        return Board(boardRepresentation: transposedRepresentation)
        
    }
}















