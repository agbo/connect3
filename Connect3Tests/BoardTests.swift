import XCTest

@testable import Connect3

class BoardTests: XCTestCase {
    
    var board : Board!
    
    override func setUp() {
        super.setUp()
        
        board = Board()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
    
    }
    
    
    func testCanInitializeBoard(){
        
        
        XCTAssertNotNil(board)
    }
    
    
    func
        testAfterAddingCoinItsInTheRightPosition(){
        
        
        board.playAt(col: 0, player: .Red)
        
        var player = board.playerAt(col: 0, row:0)
        
        XCTAssertEqual(player, .Red)
        
        
        board.playAt(col: 0, player: .White)
        
        player = board.playerAt(col: 0, row:1)
        
        XCTAssertEqual(player, .White)
        
        
    }
    
    func testBoardWithEmptyPosition_isOccupiedByEmptyPlayer(){
    
        
        XCTAssertEqual(board.playerAt(col: 0, row: 0), .Empty)
        
        XCTAssertEqual(board.playerAt(col: 1, row: 2), .Empty)
        
        XCTAssertEqual(board.playerAt(col: 3, row: 4), .Empty)
        
    }
    
    func testBoardWithPlayer_hasPositionOccupiedByPlayer(){
        
        // Lo mismo de arriba, pero para white y red
        
    }
    
    func testFullColumn_DoesntAcceptNewCoins(){
        
        
        board.playAt(col: 0, player: .Red)
        board.playAt(col: 0, player: .White)
        board.playAt(col: 0, player: .Red)
        board.playAt(col: 0, player: .White)
        board.playAt(col: 0, player: .Red)
        
        let alreadyFullBoard : Board! = board
        
        board.playAt(col: 0, player: .White)
        
        
        XCTAssertEqual(board, alreadyFullBoard)
        
    }
    
    func test2EqualBoards_areEqual(){
        
        // Empty board
        let emptyBoard = board
        XCTAssertEqual(emptyBoard, board)
        
        
        // Some non-empty boards
        var board2 = board
        
        board.playAt(col: 0, player: .Red)
        board2?.playAt(col: 0, player: .Red)
        
        XCTAssertEqual(board2, board)
        
        // Comprobamos la aserción inversa
        XCTAssertNotEqual(emptyBoard, board)
        XCTAssertNotEqual(emptyBoard, board2)
        
        board2?.playAt(col: 3, player: .White)
        XCTAssertNotEqual(board, board2)
        
    }
    
    func testEqualBoards_haveSameHash(){
        
        // caso vacio
        var board2 : Board! = board
        
        XCTAssertEqual(board2.hashValue, board.hashValue)
        
        // Caso con jugadas
        board.playAt(col: 2, player: .Red)
        board2?.playAt(col: 2, player: .Red)
        
        XCTAssertEqual(board2.hashValue, board.hashValue)
        
        // Inversa
        board2?.playAt(col: 3, player: .White)
        
        XCTAssertNotEqual(board2.hashValue, board.hashValue)
        
        
    }
    
    
    func testPlayOutOfBounds_isNOP(){
        
        let oldBoard = board
        
        board.playAt(col: Board.width + 9, player: .Red)
        XCTAssertEqual(board, oldBoard)
        
        board.playAt(col: -42, player: .White)
        XCTAssertEqual(board, oldBoard)
        
    }
    
    
    func testBoardWith3InColumn_isAWin(){
        
        board.playAt(col: 0, player: .Red)
        board.playAt(col: 0, player: .White)
        board.playAt(col: 0, player: .Red)
        board.playAt(col: 0, player: .Red)
        board.playAt(col: 0, player: .Red)
        
        let r = Connect3Rules(board: board)
        XCTAssertEqual(r.winner(), .Red)
        XCTAssertNotEqual(r.winner(), .White)
        XCTAssertNotEqual(r.winner(), .Empty)
        
        
    }
    
    func testTransposedOfTransposed_isOriginalBoard(){
        
        board.playAt(col: 0, player: .White)
        board.playAt(col: 1, player: .White)
        board.playAt(col: 2, player: .Red)
        board.playAt(col: 2, player: .White)
        board.playAt(col: 3, player: .Red)
        board.playAt(col: 4, player: .White)
        board.playAt(col: 4, player: .Red)
        
        XCTAssertEqual(board.transposed().transposed(), board)
        
    }
}












