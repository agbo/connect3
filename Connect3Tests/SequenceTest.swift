

import XCTest

class SequenceTest: XCTestCase {
    

    func testMap(){
        
        // Map tansforma arrays
        let original = [1,2,3,4]
        let result = [2,3,4,5]
        
        XCTAssertEqual(result, original.map{$0 + 1})
    }
    
    func testFlatMap(){
        
        // FlatMap, aplasta y luego mapea arrays
        let original   = [[1], [2,3],[4]]
        let result  = [2,3,4,5]
        
        XCTAssertEqual(result, (original.flatMap{$0}).map{$0 + 1})
    }
}
